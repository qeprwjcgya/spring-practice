package com.spring.practice.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

/**
 * <p>Description: </p>
 * <p>Copyright(c) 2015-2016 cadyd.com Inc. All Rights Reserved.</p>
 * <p>Other: </p>
 * <p>Date：2017-06-12 14:29 </p>
 * <p>Modification Record 1: </p>
 * <pre>
 *  Modified Date：
 *  Version：
 *  Modifier：
 *  Modification Content：
 * </pre>
 * <p>Modification Record 2：…</p>
 *
 * @author <a href="wubin3347@gmail.com">wubin</a>
 * @version 1.0.0
 */


@SpringBootApplication
public class PracticeApiApplication {
  public static void main(String[] args) {
    SpringApplication.run(PracticeApiApplication.class, args);
  }

  @Autowired
  void setEnvironment(Environment env) {
    System.out.println("my-config.appName from env: " + env.getProperty("my-config.appName"));
  }
}
