package com.stress.test.user;

import com.stress.test.util.HttpTool;

import java.util.concurrent.*;

/**
 * <p>Description: </p>
 * <p>Copyright(c) 2015-2016 cadyd.com Inc. All Rights Reserved.</p>
 * <p>Other: </p>
 * <p>Date：2017-07-11 23:02 </p>
 * <p>Modification Record 1: </p>
 * <pre>
 *  Modified Date：
 *  Version：
 *  Modifier：
 *  Modification Content：
 * </pre>
 * <p>Modification Record 2：…</p>
 *
 * @author <a href="wubin3347@gmail.com">wubin</a>
 * @version 1.0.0
 */


/**
 * 异步执行HTTP请求
 */
public class HttpCallable implements Callable<String> {

  private String url;

  private String jsonParams;

  private int startIndex = 10000000;

  public HttpCallable(String url, String jsonParams) {
    this.url = url;
    this.jsonParams = jsonParams;
  }

  /**
   * 执行并返回结果
   */

  public String call() throws Exception {
    return HttpTool.requestPost(url, jsonParams);
  }



  /**
   * 模拟并发测试
   */
  public static void main(String[] args) throws InterruptedException, ExecutionException {
    //模拟并发数
    int concurrencyNumber = 20000;

    //    List<String> resList = Lists.newArrayList();

    //    List<String> resList = new ArrayList<String>();

    //执行线程池
    ExecutorService ex = Executors.newFixedThreadPool(500);


    String url = "http://118.31.68.251:10010/user/register";

    int index = 0;
    for (int i = 0; i < concurrencyNumber; i++) {
      Future<String> callRes = ex.submit(new HttpCallable(url, ParaJson.JSON.getParaJson()));
      //      resList.add(callRes.get());
      if (callRes.get() != null) {
        index++;
      }
    }

    System.out.println(index);

  }



}
