package com.stress.test.user;

/**
 * <p>Description: </p>
 * <p>Copyright(c) 2015-2016 cadyd.com Inc. All Rights Reserved.</p>
 * <p>Other: </p>
 * <p>Date：2017-07-11 23:35 </p>
 * <p>Modification Record 1: </p>
 * <pre>
 *  Modified Date：
 *  Version：
 *  Modifier：
 *  Modification Content：
 * </pre>
 * <p>Modification Record 2：…</p>
 *
 * @author <a href="wubin3347@gmail.com">wubin</a>
 * @version 1.0.0
 */
public enum ParaJson {
  JSON;
  private int startIndex = 10000000;

  public synchronized String getParaJson() {
    return "{\"accountNo\":\"130" + (++startIndex) + "\",\"pwd\":\"123456\",\"code\":\"123456\"}";
  }
}
