package com.stress.test.user;

import com.stress.test.util.HttpTool;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Description: </p>
 * <p>Copyright(c) 2015-2016 cadyd.com Inc. All Rights Reserved.</p>
 * <p>Other: </p>
 * <p>Date：2017-07-11 21:48 </p>
 * <p>Modification Record 1: </p>
 * <pre>
 *  Modified Date：
 *  Version：
 *  Modifier：
 *  Modification Content：
 * </pre>
 * <p>Modification Record 2：…</p>
 *
 * @author <a href="wubin3347@gmail.com">wubin</a>
 * @version 1.0.0
 */
public class UserRegister {
  public void test() throws Exception {
    String url = "http://118.31.68.251:10010/user/register";
    Map<String, String> params = new HashMap<String, String>();
    params.put("accountNo", "13010000000");
    params.put("pwd", "123456");
    params.put("code", "123456");
    //params.put("nickName","");

    String para = "{\"accountNo\":\"13010000000\",\"pwd\":\"123456\",\"code\":\"123456\"}";

    String value = HttpTool.requestPost(url, para);


    System.out.println(value);
  }

  public static void main(String[] args) throws Exception {
    UserRegister user = new UserRegister();
    user.test();
  }
}
